const sql = require("./db.js");
const mercadopago = require ('mercadopago');

const MercadoPago = articulos => {

}

MercadoPago.mercadopagoAdd = (data) => {
   return new Promise((resolve, reject) => {

   	mercadopago.configure({
		  access_token: 'APP_USR-3064453345134395-120320-e01866d223b4c565a3f328ac8ce07fbc-681744925'
		});

   	let preference = {
   		back_urls: {
        success: 'https://ky-fetish.com/#/procesar.pedido.success',
        failure: 'https://ky-fetish.com/#/procesar.pedido.success',
        pending: 'https://ky-fetish.com/#/procesar.pedido.success'
      },
      auto_return: 'approved',
			items: data.productos,
			shipments:{
				cost: data.envio,
				mode: "not_specified",
			}
		};

		mercadopago.preferences.create(preference)
		.then(function(response){
			// Este valor reemplazará el string "<%= global.id %>" en tu HTML
		  resolve(response.body.id)
		}).catch(function(error){
			reject(error);
		});
  })
};

MercadoPago.agregarPagoDirecciones = (d,idcliente,idpago) => {
	return new Promise((resolve, reject) => {
		sql.query(`INSERT INTO pago_direcciones(idcliente,idpago,colonia,nombre,apellido,estado,municipio,calle,numero,cp,email,telefono,tipo)
			VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)`,[idcliente,idpago,d.colonia ,d.nombre,d.apellido, d.estado, d.municipio,
	                                    d.calle,d.numero,d.cp,d.email,d.telefono,d.tipo], (err, res) => {	
	    if (err) {
	    	reject(err);
	    	return;
	    }
	    console.log(res.insertId)
	    resolve(res.insertId)
		});
	})
};

MercadoPago.agregarPagoDireccionesFac = (d,idcliente,idpago) => {
	return new Promise((resolve, reject) => {
		sql.query(`INSERT INTO pago_facturacion(idcliente,idpago,colonia,nombre,apellido, estado, municipio,calle,numero,cp,email,telefono,tipo)
		VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)`,[idcliente,idpago,d.colonia ,d.nombre,d.apellido, d.estado, d.municipio,
	                                    d.calle,d.numero,d.cp,d.email,d.telefono,d.tipo], (err, res) => {	
	    if (err) {
	    	reject(err);
	    	return;
	    }
	    console.log(res.insertId)
	    resolve(res.insertId)
		});
	})
};

MercadoPago.agregarDocum = (docum,idcliente,idpago,iddireccionfacturacion,iddirecion,idcupon) => {
	return new Promise((resolve, reject) => {
	  sql.query(`INSERT INTO docum(idcliente,iddireccionfacturacion,iddirecion,idpago,descuento,subtotal,total,envio,envio_gratis,descuento_cupon,idcupon)
	  	VALUES(?,?,?,?,?,?,?,?,?,?,?)`,
	  	[idcliente,iddireccionfacturacion,iddirecion,idpago,docum.descuento,docum.subtotal,docum.total,docum.envio,docum.envio_gratis,docum.descuento_cupon,idcupon], (err, res) => {  
	    if (err) {
	    	reject(err);
	    	return;
	    }
	    console.log(res.insertId)
	    resolve(res.insertId)
	  });
	 });
};

MercadoPago.agregarMovim = (productos,iddocum) => {
	return new Promise((resolve, reject) => {
		productos.forEach((element,index)=>{
			console.log(element)
    	sql.query(`INSERT INTO movim(iddocum,id,foto,cantidad,precio1,pjedesc)
    		VALUES(?,?,?,?,?,?)`,
		  	[iddocum, element.id ,element.foto ,element.cantidad ,element.precio1 ,element.pjedesc], (err, res) => {  
		    if (err) {
		      reject(err);
		      return;
		    }
			  if(index == (productos.length-1)){
	    		resolve(res.insertId)
			  }
		  });
  	})
  })
};


MercadoPago.validarExistePago = (collection_id, result)=>{
	sql.query(`SELECT * FROM pagos_mercadopago WHERE collection_id = ? `, [collection_id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("PEDIDOS: ", res);
    result(null, res);
	})
};

MercadoPago.addPagoMercadopago = (p, result) => {
  sql.query(`INSERT INTO pagos_mercadopago (collection_id,collection_status,merchant_order_id,payment_id,payment_type,preference_id,status)
                                  VALUES(?,?,?,?,?,?,?)`,
    [p.collection_id,p.collection_status,p.merchant_order_id,p.payment_id,p.payment_type,p.preference_id,p.status], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear almacen: ", { id: res.insertId, ...p });
    result(null, { id: res.insertId, ...p });
  });
};

MercadoPago.updateMercadoPago = (id, result) => {
	return new Promise((resolve, reject) => {
  sql.query(`UPDATE docum SET estatus = 1 WHERE iddocum > 0 AND idpago = ?`, [id],(err, res) => {
    if (err) {
     	reject(err)
      return;
    }
    if (res.affectedRows == 0) {
      reject(err)
      return;
    }
    resolve(res)
  });
	})
};


MercadoPago.getIdMovim = (id, result) => {
	return new Promise((resolve, reject) => {
	  sql.query(`SELECT * FROM docum WHERE idpago = ?`, [id],(err, res) => {
	    if (err) {
	     	reject(err)
	      return;
	    }
	    resolve(res[0])
	  });
	})
};

MercadoPago.getArtsDocum = (id, result) => {
	return new Promise((resolve, reject) => {
	  sql.query(`SELECT a.codigo, m.id, m.cantidad FROM movim m 
				LEFT JOIN arts a ON a.id = m.id WHERE iddocum = ?`, [id],(err, res) => {
	    if (err) {
	     	reject(err)
	      return;
	    }
	    resolve(res)
	  });
	})
};

MercadoPago.updateArt = (art, result) => {
	return new Promise((resolve, reject) => {
	  sql.query(`UPDATE almacen SET cant = cant - ? WHERE idalmacen > 0 AND codigo = ?`, [art.cantidad, art.codigo],(err, res) => {
	    if (err) {
	     	reject(err)
	      return;
	    }
	    resolve(res)
	  });
	})
};

MercadoPago.addSalida = (art, result) => {
	return new Promise((resolve, reject) => {
	  sql.query(`INSERT INTO salidas (codigo,cant)VALUES(?,?)`,[art.codigo, art.cantidad], (err, res) => { 
	    if (err) {
	     	reject(err)
	      return;
	    }
	    resolve(res)
	  });
	})
};

MercadoPago.getRelacionadosSub = (id) => {
   return new Promise((resolve, reject) => {
    
  })
};

MercadoPago.obtener_pedidos_cliente = (id, result)=>{
	sql.query(`SELECT * FROM docum WHERE idcliente=? `, [id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("PEDIDOS: ", res);
    result(null, res);
	})
};

MercadoPago.obtener_productos_movim = (id, result)=>{
	sql.query(`SELECT m.*, a.nomart FROM movim m LEFT JOIN arts a ON m.id = a.id WHERE iddocum  = ?`, [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("NOVEDADES: ", res);
    result(null, res);
  });
};

MercadoPago.deleteDirecciones = (id, result) => {
	return new Promise((resolve, reject) => {
	  sql.query("DELETE FROM pago_direcciones where idpago_direccion = ?;",[id], (err, res) => {
	    if (err) {
	    	reject(err);
	    	return;
	    }
	    resolve(res)
	  });
	});
};

MercadoPago.deleteDireccionesFac = (id, result) => {
	return new Promise((resolve, reject) => {
	  sql.query("DELETE FROM pago_facturacion where idpagofacturacion = ?;",[id], (err, res) => {
	    if (err) {
	    	reject(err);
	    	return;
	    }
	    resolve(res)
	  });
	});
};

MercadoPago.deleteDocum = (id, result) => {
	return new Promise((resolve, reject) => {
	  sql.query("DELETE FROM docum where iddocum = ?;",[id], (err, res) => {
	    if (err) {
	    	reject(err);
	    	return;
	    }
	    resolve(res)
	  });
	});
};

MercadoPago.deleteMovim = (id, result) => {
	return new Promise((resolve, reject) => {
	  sql.query("DELETE FROM movim where idmovim > 0 AND iddocum = ?;",[id], (err, res) => {
	    if (err) {
	    	reject(err);
	    	return;
	    }
	    resolve(res)
	  });
	});
};


MercadoPago.getRastreo = (idpago, result)=>{
	sql.query(`SELECT * FROM rastreo WHERE idpago = ? `, [idpago], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("PEDIDOS: ", res);
    result(null, res);
	})
};

MercadoPago.addRastreo = (p, result) => {
  sql.query(`INSERT INTO rastreo (idpago,carrier,currency,currentBalance,label,service,totalPrice,trackUrl,trackingNumber)
                                  VALUES(?,?,?,?,?,?,?,?,?)`,
    [p.idpago,p.carrier,p.currency,p.currentBalance,p.label,p.service,p.totalPrice,p.trackUrl,p.trackingNumber], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear almacen: ", { id: res.insertId, ...p });
    result(null, { id: res.insertId, ...p });
  });
};

module.exports = MercadoPago;