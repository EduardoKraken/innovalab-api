const sql = require("./db.js");

// constructor
const Banners = bann => {
    this.idbanners   = bann.idbanners;
    this.nombanner   = bann.nombanner;
};

// Agregar un laboratorio
Banners.addBanner = (newBanner, result) => {
	sql.query(`INSERT INTO banners(nombanner)VALUES(?);`,
						 [newBanner.nombanner], (err, res) => {	
    if (err) {
	    console.log("error: ", err);
	    result(err, null);
	    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Banners: ", { id: res.insertId, ...newBanner });
    result(null, { id: res.insertId, ...newBanner });
	});
};


// Traer laboratior activos
Banners.getBanners = result => {
  sql.query('SELECT * FROM banners;', (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Banners.getMarcas = result => {
  sql.query('SELECT * FROM marcas WHERE deleted = 0;', (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Banners.deleteBanner = (id, result) => {
  sql.query("DELETE FROM banners where idbanners = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};


module.exports = Banners;