const sql = require("./db.js");

const Articulos = articulos => {
    this.id             = articulos.id;
    this.nomart         = articulos.nomart;
    this.codigo         = articulos.codigo;
    this.idlaboratorio  = articulos.idlaboratorio;
    this.nomlab         = articulos.nomlab;
    this.descrip        = articulos.descrip;
    this.statusweb      = articulos.statusweb;
    this.precio1        = articulos.precio1;
    this.sal            = articulos.sal;
    this.novedades      = articulos.novedades;
    this.destacados     = articulos.destacados;
    this.pjedesc        = articulos.pjedesc;
    this.alto           = articulos.alto;   
    this.largo          = articulos.largo;
    this.ancho          = articulos.ancho;
    this.peso           = articulos.peso;
}

Articulos.obtener_articulos_random = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.precio1, a.precio2, a.pjedesc, a.largo, a.ancho, a.alto, a.peso, a.envoltorio,
                (SELECT image_name FROM fotos f 
                  WHERE f.codigo = a.codigo AND f.principal = 1 
                  ORDER BY f.principal 
                  DESC 
                  LIMIT 1) AS foto
              FROM arts a WHERE a.estatus = 1 ORDER BY RAND() LIMIT 12;`, (err, res) => {
                  if (err) { result(null, err);  return; }
    console.log("ARTICULOS RANDOM: ", res);
    result(null, res);
  });
};


Articulos.obtener_articulos = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.precio1, a.precio2, a.pjedesc, a.largo, a.ancho, a.alto, a.peso, a.envoltorio,
                (SELECT image_name FROM fotos f 
                  WHERE f.codigo = a.codigo AND f.principal = 1 
                  ORDER BY f.principal 
                  DESC 
                  LIMIT 1) AS foto
              FROM arts a WHERE a.estatus = 1;`, (err, res) => {
                  if (err) { result(null, err);  return; }
    console.log("ARTICULOS RANDOM: ", res);
    result(null, res);
  });
};

Articulos.getAll = result => {
  sql.query(`SELECT a.*, f.familia, IFNULL(c.categoria,'NA') AS categoria, IFNULL(s.subcategoria,'NA') AS subcategoria FROM arts a 
    LEFT JOIN categorias c ON c.idcategorias = a.idcategorias
    LEFT JOIN familias f ON f.idfamilias = a.idfamilias
    LEFT JOIN subcategorias s ON s.idsubcategorias = a.idsubcategorias;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getArticulosActivo = result => {
  sql.query(`SELECT a.*, f.familia, IFNULL(c.categoria,'NA') AS categoria, IFNULL(s.subcategoria,'NA') AS subcategoria FROM arts a 
    LEFT JOIN categorias c ON c.idcategorias = a.idcategorias
    LEFT JOIN familias f ON f.idfamilias = a.idfamilias
    LEFT JOIN subcategorias s ON s.idsubcategorias = a.idsubcategorias WHERE a.estatus = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getArticuloId = (id,result) => {
  sql.query(`SELECT a.*, f.familia, IFNULL(c.categoria,'NA') AS categoria, IFNULL(s.subcategoria,'NA') AS subcategoria FROM arts a 
    LEFT JOIN categorias c ON c.idcategorias = a.idcategorias
    LEFT JOIN familias f ON f.idfamilias = a.idfamilias
    LEFT JOIN subcategorias s ON s.idsubcategorias = a.idsubcategorias WHERE a.id = ?;`,[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res[0]);
    result(null, res[0]);
  });
};

Articulos.getNovedades = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, 
    a.destacados, a.pjedesc FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.novedades = 1 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getDestacados = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, 
    a.destacados, a.pjedesc FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.destacados = 1 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.obtener_novedades = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.precio1, a.precio2, a.pjedesc, a.largo, a.ancho, a.alto, a.peso, a.envoltorio,
                    a.novedades, a.descrip, a.estatus,
                      (SELECT image_name FROM fotos f WHERE f.codigo = a.codigo AND f.principal = 1 ORDER BY f.principal DESC LIMIT 1) AS foto
                  FROM arts a WHERE a.estatus =1 AND a.novedades = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("NOVEDADES: ", res);
    result(null, res);
  });
};

Articulos.obtener_destacados = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.precio1, a.precio2, a.pjedesc, a.largo, a.ancho, a.alto, a.peso, a.envoltorio,
              a.destacados, a.descrip, a.estatus,
                  (SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1) AS foto
              FROM arts a WHERE a.estatus =1 AND a.destacados = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("DESTACADO: ", res);
    result(null, res);
  });
};

Articulos.obtener_juguetes = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.precio1, a.precio2, a.pjedesc, 
                a.juguete, a.descrip, a.estatus,
                  (SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1) AS foto
              FROM arts a WHERE a.estatus =1 AND a.juguete = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("JUGUETES: ", res);
    result(null, res);
  });
};




Articulos.getArtxLab =(id, result) => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio 
    WHERE  a.statusweb = 1 AND a.idlaboratorio = ?;`,[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getPromociones = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.pjedesc > 0 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};


Articulos.fotosArt = result => {
  sql.query(`SELECT idfotos, codigo, image_name, principal FROM fotos`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.fotosxArt = (codigo, result) => {
  sql.query(`SELECT idfotos, codigo, image_name, principal FROM fotos WHERE codigo = ?`,[codigo], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.addFoto = (foto, result) => {
  sql.query(`INSERT INTO fotos(codigo, image_name, principal)VALUES(?,?,?)`,
    [foto.codigo,foto.image_name,foto.principal], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...foto });
    result(null, { id: res.insertId, ...foto });
  });
};


Articulos.addArticulos = (art, result) => {
  sql.query(`INSERT INTO arts(nomart,codigo,idfamilias,idcategorias,idsubcategorias,descrip,descripLarga)VALUES(?,?,?,?,?,?,?)`,
    [art.nomart,art.codigo,art.idfamilias,art.idcategorias,art.idsubcategorias,art.descrip,art.descripLarga], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...art });
    result(null, { id: res.insertId, ...art });
  });
};


Articulos.getArticuloCodigo = (codigo, result) => {
  sql.query(`SELECT a.*, f.familia, c.categoria,s.subcategoria FROM arts a 
    LEFT JOIN categorias c ON c.idcategorias = a.idcategorias
    LEFT JOIN familias f ON f.idfamilias = a.idfamilias
    LEFT JOIN subcategorias s ON s.idsubcategorias = a.idsubcategorias
    WHERE a.codigo = ? OR a.nomart like '%` + codigo +`%';`,[ codigo ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    console.log("found students: ", res);
    result(null, res);
  });
};


Articulos.getArticuloTienda = (codigo, result) => {
  sql.query(`SELECT a.*, f.familia, c.categoria,s.subcategoria FROM arts a 
    LEFT JOIN categorias c ON c.idcategorias = a.idcategorias
    LEFT JOIN familias f ON f.idfamilias = a.idfamilias
    LEFT JOIN subcategorias s ON s.idsubcategorias = a.idsubcategorias
    WHERE a.codigo = ? OR a.nomart like '%` + codigo +`%';`,[ codigo ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }

    console.log("found students: ", res);
    result(null, res);
  });
};


Articulos.updateArticulos = (id, art, result) => {
  sql.query(` UPDATE arts SET nomart = ?, idfamilias = ?, idcategorias = ?, idsubcategorias =?, estatus = ?, descrip = ?, descripLarga =?,
             precio1 = ?, pjedesc = ?, peso = ?, largo =?, ancho = ?, alto =?,envoltorio=? WHERE id = ?;`, 
    [art.nomart, art.idfamilias, art.idcategorias,art.idsubcategorias,art.estatus,art.descrip,art.descripLarga,art.precio1,art.pjedesc,art.peso,art.largo,art.ancho, art.alto,art.envoltorio,  id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated articulos: ", { id: id, ...art });
      result(null, { id: id, ...art });
    }
  );
};

Articulos.updateNovedades = (id, art, result) => {
  sql.query(` UPDATE arts SET novedades = ?, destacados = ?, juguete  = ? WHERE id = ?;`, [art.novedades,art.destacados, art.juguete, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated articulos: ", { id: id, ...art });
      result(null, { id: id, ...art });
    }
  );
};

Articulos.deleteFoto = (id, result) => {
  sql.query("DELETE FROM fotos where idfotos = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};

Articulos.updateFotoPrincipal = (id, foto, result) => {
  sql.query(` UPDATE fotos SET principal = ?  WHERE idfotos = ?;`, [foto.principal, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated fotoiculos: ", { id: id, ...foto });
      result(null, { id: id, ...foto });
    }
  );
};


Articulos.getArticuloFotos = (codigo, result) => {
  sql.query(`SELECT * FROM fotos WHERE codigo = ? ORDER BY principal DESC`,[ codigo ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    result(null, res);
  });
};

Articulos.getArticuloFam = (id) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT f.*, fa.familia FROM familias_art f
        LEFT JOIN familias fa ON fa.idfamilias = f.idfamilias WHERE f.idarticulo = ? `,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};

Articulos.getArticuloCat = (id) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT c.*, ca.categoria FROM categorias_art c
      LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idarticulo = ?`,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};

Articulos.getArticuloSub = (id) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT s.*, sa.subcategoria FROM subcategorias_art s
      LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idarticulo = ?`,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};



Articulos.getArticuloxFam = (id, result) => {
  sql.query(`SELECT f.*, fa.familia, a.*, IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto FROM familias_art f
LEFT JOIN arts a ON a.id = f.idarticulo
      LEFT JOIN familias fa ON fa.idfamilias = f.idfamilias WHERE f.idfamilias = ?  AND a.estatus = 1`,[ id ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    result(null, res);
  })
};

Articulos.getArticuloxCat = (id, result) => {
  sql.query(`SELECT c.*, ca.categoria, a.*, IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto FROM categorias_art c
      LEFT JOIN arts a ON a.id = c.idarticulo
    LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idcategorias = ? AND a.estatus = 1`,[ id ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    result(null, res);
  })
};

Articulos.getArticuloxSub = (id, result) => {
  sql.query(`SELECT s.*, sa.subcategoria, a.*, IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto FROM subcategorias_art s
    LEFT JOIN arts a ON a.id = s.idarticulo
    LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idsubcategorias = ? AND a.estatus = 1`,[ id ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    result(null, res);
  })
};

Articulos.getRelacionadosCat = (id) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT c.*, ca.categoria, a.*,IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto  FROM categorias_art c
      LEFT JOIN arts a ON a.id = c.idarticulo
      LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idcategorias IN(
        SELECT c.idcategorias FROM categorias_art c
        LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idarticulo = ?) AND a.estatus = 1;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};

Articulos.getRelacionadosSub = (id) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT s.*, sa.subcategoria, a.*,IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto  FROM subcategorias_art s
    LEFT JOIN arts a ON a.id = s.idarticulo
    LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idsubcategorias IN(
    SELECT sa.idsubcategorias FROM subcategorias_art s
      LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idarticulo = ? ) AND a.estatus = 1;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};


module.exports = Articulos;