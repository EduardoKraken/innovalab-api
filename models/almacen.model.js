const sql = require("./db.js");

// constructor

const Alamacen = almacen => {
  this.codigo      = almacen.codigo;
  this.lote        = almacen.lote;
  this.cant        = almacen.cant;
  this.caducidad   = almacen.caducidad;
};

Alamacen.addAlmacen = (ent, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`INSERT INTO almacen (codigo,lote,cant,caducidad)
                                  VALUES(?,?,?,?)`,
      [ent.codigo,ent.lote,ent.cant,ent.caducidad], (err, res) => { 
      if (err) {
        reject(err)
        return;
      }
      resolve(res)
    });
  })
};

Alamacen.getAlmacen = (art, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM almacen WHERE codigo = ?`, [art.codigo],(err, res) => {
      if (err) {
        reject(err)
        return;
      }
      resolve(res)
    });
  })
};

Alamacen.updateAlmacen2 = (art, result) => {
  return new Promise((resolve, reject) => {
    sql.query(`UPDATE almacen SET cant = cant + ? WHERE idalmacen > 0 AND codigo = ?`, [art.cant, art.codigo],(err, res) => {
      if (err) {
        reject(err)
        return;
      }
      resolve(res)
    });
  })
};

// Alamacen.addAlmacen = (ent, result) => {
//   sql.query(`INSERT INTO almacen (codigo,lote,cant,caducidad)
//                                   VALUES(?,?,?,?)`,
//     [ent.codigo,ent.lote,ent.cant,ent.caducidad], (err, res) => {  
//     if (err) {
//       console.log("error: ", err);
//       result(err, null);
//       return;
//     }
//     // console.log("Crear Grupo: ", res.insertId);
//     console.log("Crear almacen: ", { id: res.insertId, ...ent });
//     result(null, { id: res.insertId, ...ent });
//   });
// };

Alamacen.getAlmacenList = result => {
  sql.query(`SELECT a.*, ar.nomart FROM almacen a 
    LEFT JOIN arts ar ON ar.codigo = a.codigo;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("almacen: ", res);
    result(null, res);
  });
};

Alamacen.updateAlmacen = (id, alm, result) => {
  sql.query(`UPDATE almacen SET cant = ? WHERE idalmacen = ? `, [alm.cant, id],(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("updated almacen: ", { id: id, ...alm });
    result(null, { id: id, ...alm });
  });
};

Alamacen.validarAlmacen = (ent, result) => {
  sql.query(`SELECT a.codigo, a.id, SUM(alm.cant) AS cant , a.nomart, 
            (SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1) AS foto 
    FROM almacen alm LEFT JOIN arts a ON a.codigo = alm.codigo WHERE a.id IN (?) GROUP BY a.codigo, a.id;`,
    [ent], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("almacen: ", res);
    result(null, res);
  });
};

module.exports = Alamacen;