var https = require('follow-redirects').https;
var fs = require('fs');

// constructor
const Envios = bann => {
    this.idbanners   = bann.idbanners;
    this.nombanner   = bann.nombanner;
};

// Agregar un laboratorio
Envios.pruebasEnvios = (params, result) => {
	const params_body = params
	const body_params = params_body.body
	var options = {
	  'method': params_body.metodo,
	  'hostname': 'queries.envia.com',
	  'path': params_body.path,
	  'headers': {
	    'Content-Type': 'application/json',
	    'Authorization': 'Bearer 798e40184c79642bd34b9678ad776e1ceeda2a9cd47ef3fb311c0dfcf6cf79b6'
	  },
	  'maxRedirects': 20
	};

	var req = https.request(options, function (res) {
	  var chunks = [];

	  res.on("data", function (chunk) {
	    chunks.push(chunk);
	  });

	  res.on("end", function (chunk) {
	    var body = Buffer.concat(chunks);
	    result(null, body)
	  });

	  res.on("error", function (error) {
	    result(error, null)
	  });
	});

	var postData = JSON.stringify({
	  body_params
	});

	req.write(postData);

	req.end();
};

module.exports = Envios;