const sql = require("./db.js");

// constructor
const Laboratorio = function(lab) {
    this.idlaboratorios   = lab.idlaboratorios;
    this.nomlab           = lab.nomlab;
    this.estatus          = lab.estatus;
};

// Agregar un laboratorio
Laboratorio.addCategorias = (n, result) => {
	sql.query(`INSERT INTO categorias(categoria,idfamilias,estatus, foto)VALUES(?,?,?,?)`,
						 [n.categoria,n.idfamilias,n.estatus,n.foto], (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
	});
};

// Traer laboratorios por id
Laboratorio.getCategorias = (result)=>{
	sql.query(`SELECT c.*, f.familia FROM categorias c 
    LEFT JOIN familias f ON f.idfamilias = c.idfamilias
    ORDER BY c.categoria;`, (err,res)=>{
		if (err) {
      result(null, err);
      return;
    }
    result(null, res);
	})
};

// Actualizar laboratorios
Laboratorio.updateCategorias = (id, n, result) => {
  sql.query(` UPDATE categorias SET categoria = ?, idfamilias = ? , estatus = ?, foto = ?
                WHERE idcategorias = ?`, [n.categoria,n.idfamilias, n.estatus, n.foto, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...n });
    }
  );
};


// Traer laboratior activos
Laboratorio.getCategoriasActivo = result => {
  sql.query(`SELECT * FROM categorias WHERE estatus = 1 ORDER BY categoria`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

// Traer laboratior activos
Laboratorio.getLabList = result => {
  sql.query(`SELECT * FROM laboratorios ORDER BY nomlab`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

// Traer laboratorios por id
Laboratorio.getCategoriaPorFamilia = (familia,result)=>{
  sql.query(`SELECT * FROM categorias WHERE idfamilias = ?;`,[familia], (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};



Laboratorio.addCategoriasArt = (n, result) => {
  sql.query(`INSERT INTO categorias_art(idcategorias,idarticulo)VALUES(?,?)`,
             [n.idcategorias,n.idarticulo], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};

Laboratorio.getCategoriasArtId = (id,result)=>{
  sql.query(`SELECT c.*, ca.categoria FROM categorias_art c
LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idarticulo = ?`,[id],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.deleteCategoriasArt = (id, result) => {
  sql.query("DELETE FROM categorias_art WHERE idcategorias_art = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};

/***********************************************************************/

Laboratorio.getCatActivos = (result)=>{
  sql.query("SELECT * FROM laboratorios WHERE estatus = 1 ORDER BY nomlab", (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};


Laboratorio.getSubcategorias = (result)=>{
  sql.query(`SELECT s.*, c.categoria, f.familia FROM subcategorias s
    LEFT JOIN categorias c ON s.idcatego = c.idcategorias
    LEFT JOIN familias f ON f.idfamilias = c.idfamilias ORDER BY subcategoria;`, (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.getSubcategoriasActivo = (result)=>{
  sql.query(`SELECT * FROM subcategorias WHERE estatus = 1`, (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

// Agregar un laboratorio
Laboratorio.addSubcategorias = (n, result) => {
  sql.query(`INSERT INTO subcategorias(subcategoria,idcatego,estatus)VALUES(?,?,?)`,
             [n.subcategoria,n.idcatego,n.estatus], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};


// Actualizar laboratorios
Laboratorio.updateSubCatego = (id, n, result) => {
  sql.query(` UPDATE subcategorias SET subcategoria = ?, estatus = ?, idcatego = ?
                WHERE idsubcategorias = ?`, [n.subcategoria, n.estatus,n.idcatego, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...n });
    }
  );
};

Laboratorio.getSubcategoriasPorCatego = (idcatego, result)=>{
  sql.query("SELECT * FROM subcategorias WHERE idcatego = ? ORDER BY subcategoria", [idcatego], (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};


Laboratorio.addsubCategoriasArt = (n, result) => {
  sql.query(`INSERT INTO subcategorias_art(idsubcategorias,idarticulo)VALUES(?,?)`,
             [n.idsubcategorias,n.idarticulo], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};

Laboratorio.getsubCategoriasArtId = (id,result)=>{
  sql.query(`SELECT s.*, sa.subcategoria FROM subcategorias_art s
LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idarticulo = ?`,[id],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.deletesubCategoriasArt = (id, result) => {
  sql.query("DELETE FROM subcategorias_art WHERE idsubcategorias_art = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};
/***************************************************************************************************/
/*                                           FAMILIA                                               */
/***************************************************************************************************/
// Agregar un laboratorio
Laboratorio.addFamilias = (n, result) => {
  sql.query(`INSERT INTO familias(familia,estatus)VALUES(?,?)`,
             [n.familia,n.estatus], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};


// Actualizar laboratorios
Laboratorio.updateFamilias = (id, n, result) => {
  sql.query(` UPDATE familias SET familia = ?, estatus = ?
                WHERE idfamilias = ?`, [n.familia, n.estatus,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...n });
    }
  );
};

Laboratorio.getFamilias = (result)=>{
  sql.query("SELECT * FROM familias",(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.getFamiliasActivo = (result)=>{
  sql.query("SELECT * FROM familias WHERE estatus = 1",(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};


Laboratorio.addFamiliasArt = (n, result) => {
  sql.query(`INSERT INTO familias_art(idfamilias,idarticulo)VALUES(?,?)`,
             [n.idfamilias,n.idarticulo], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};

Laboratorio.getFamiliasArtId = (id,result)=>{
  sql.query(`SELECT f.*, fa.familia FROM familias_art f
      LEFT JOIN familias fa ON fa.idfamilias = f.idfamilias WHERE f.idarticulo = ?;`,[id],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.deleteFamiliasArt = (id, result) => {
  sql.query("DELETE FROM familias_art WHERE idfamilias_art = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};

/***************************************************************************************************/
/*                                           FAMILIA                                               */
/***************************************************************************************************/

Laboratorio.getMenuTienda = (result)=>{
  sql.query("SELECT * FROM familias WHERE estatus = 1",(err,familias)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    sql.query("SELECT * FROM categorias WHERE estatus = 1",(err,categorias)=>{
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      sql.query("SELECT * FROM subcategorias WHERE estatus = 1",(err,subcategorias)=>{
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }

        var menu = []

        for(const i in familias){
          var famili = {
            familia    : familias[i].familia,
            idfamilias : familias[i].idfamilias,
            categorias : []
          }
          menu.push(famili)
        }

        for(const i in menu){
          for(const j in categorias){
            if(menu[i].idfamilias == categorias[j].idfamilias){
              menu[i].categorias.push(categorias[j])
            }
          }
        }

        var menu2 = []
        for(const i in menu){
          var famili = {
            familia    : menu[i].familia,
            idfamilias : menu[i].idfamilias,
            categorias : []
          }
          for(const j in menu[i].categorias){
            var cate = {
              categoria     : menu[i].categorias[j].categoria,
              idcategorias  : menu[i].categorias[j].idcategorias,
              subcategorias : []
            }
            famili.categorias.push(cate)
              
          }
          menu2.push(famili)
        }

        for(const i in menu2){
          for(const j in menu2[i].categorias){
            for(const k in subcategorias){
              if(menu2[i].categorias[j].idcategorias == subcategorias[k].idcatego){
                menu2[i].categorias[j].subcategorias.push(subcategorias[k])
              }
            }
          }
        }

        result(null, menu2);
      })
    })
  })
};
module.exports = Laboratorio;