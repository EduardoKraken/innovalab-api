const sql = require("./db.js");

// constructor

const Entradas = entradas => {
  this.identradas     = entradas.identradas;
  this.nomart         = entradas.nomart;
  this.codigo         = entradas.codigo;
  this.fechaentr      = entradas.fechaentr;
  this.lote           = entradas.lote;
  this.cant           = entradas.cant;
  this.caducidad      = entradas.caducidad;
  this.proveedor      = entradas.proveedor;
  this.factura        = entradas.factura;
};


Entradas.addEntradas = (art, result) => {
  sql.query(`INSERT INTO entradas (fechaentr,codigo,lote,cant,caducidad, proveedor, factura)
                                  VALUES(?,?,?,?,?,?,?)`,
    [art.fechaentr,art.codigo, art.lote,art.cant,art.caducidad,art.proveedor,art.factura], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...art });
    result(null, { id: res.insertId, ...art });
  });
};

Entradas.getEntradasAll = result => {
  sql.query(`SELECT e.identradas, a.nomart, e.codigo, e.fechaentr, e.lote, e.cant, e.caducidad, e.proveedor, e.factura 
        FROM entradas e INNER JOIN arts a ON e.codigo = a.codigo;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

Entradas.getEntradasFecha = (fecha, result) => {
  sql.query(`SELECT e.identradas, a.nomart, e.codigo, e.fechaentr, e.lote, e.cant, e.caducidad, e.proveedor, e.factura 
        FROM  entradas e INNER JOIN arts a ON e.codigo = a.codigo WHERE fechaentr BETWEEN ? AND ?`,
    [fecha.inicio,fecha.fin], (err, res) => {  
    	console.log(sql)
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

Entradas.updateEntrada = (id, ent, result) => {
  sql.query(` UPDATE entradas SET lote = ?, proveedor =?, factura =?, caducidad=?
                WHERE identradas = ?`, [ent.lote, ent.proveedor,ent.factura,ent.caducidad,  id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated enticulos: ", { id: id, ...ent });
      result(null, { id: id, ...ent });
    }
  );
};

module.exports = Entradas;