const sql = require("./db.js");

// constructor

const Existencias = existencias => {
  this.idexistencias  = existencias.idexistencias;
  this.nomart         = existencias.nomart;
  this.codigo         = existencias.codigo;
  this.lote           = existencias.lote;
  this.cant           = existencias.cant;
  this.caducidad      = existencias.caducidad;
  this.suma           = existencias.suma;
  this.lab            = existencias.lab;
  this.sal            = existencias.sal;
};



Existencias.getExistencias = result => {
  sql.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    sql.query(`SELECT a.idalmacen, a.codigo, sum(a.cant) AS suma, ar.nomart, ar.codigo  FROM almacen a
      LEFT JOIN arts ar ON a.codigo = ar.codigo
      GROUP BY  ar.codigo;`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log("entradas: ", res);
      result(null, res);
    });
  });

};

Existencias.getExistenciaCodigo =(codigo, result) => {
  sql.query(`SELECT ar.nomart, a.idalmacen, a.codigo, a.lote, a.caducidad, a.cant, ar.sal, l.nomlab AS "lab"
        FROM almacen a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
        l.idlaboratorios = ar.idlaboratorio WHERE a.codigo = ? AND a.cant > 0  OR  ar.nomart like'%`+ codigo +`%' AND a.cant > 0
        ORDER BY a.caducidad ASC, a.cant DESC ;`,[codigo], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

Existencias.getExistenciaCodigoDes =(codigo, result) => {
  sql.query(`SELECT a.lote, ar.nomart, l.nomlab AS "lab",a.caducidad, SUM(a.cant) AS cant, ar.sal
        FROM almacen a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
        l.idlaboratorios = ar.idlaboratorio 
        WHERE a.codigo = ? AND a.cant > 0  OR  ar.nomart like"%`+codigo+`%" AND a.cant > 0
        GROUP BY a.lote, ar.nomart, l.nomlab, ar.sal,a.caducidad;`,[codigo], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};


Existencias.getExistenciaTotal = result => {
  sql.query(`SELECT ar.nomart, a.idalmacen, a.codigo, a.lote, a.caducidad, a.cant, ar.sal, l.nomlab AS "lab"
    FROM almacen  a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
    l.idlaboratorios = ar.idlaboratorio WHERE a.cant > 0 ORDER BY a.caducidad ASC, a.cant DESC;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("entradas: ", res);
    result(null, res);
  });
};

module.exports = Existencias;