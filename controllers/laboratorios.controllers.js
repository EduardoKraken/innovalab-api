
const Laboratorio = require("../models/laboratorios.model.js");
const { v4: uuidv4 } = require('uuid')

// Agregar un laboratorio
exports.addCategorias = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Laboratorio.addCategorias(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Laboratorio"
  		})
  	else res.send(data)

  })
};

// Traer laboratorios por id
exports.getCategorias = (req,res) => {
    Laboratorio.getCategorias((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

exports.getCategoriaPorFamilia = (req,res) => {
    Laboratorio.getCategoriaPorFamilia(req.params.id,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

// Actualizar un laboratorio
exports.updateCategorias = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Laboratorio.updateCategorias(req.params.id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el laboratorio con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el laboratorio con el id" + req.params.id 
				});
			}
		} 
		else res.send(data);
	}
	);
}

// Traer laboratorios activos
exports.getCategoriasActivo = (req,res) => {
    Laboratorio.getCategoriasActivo((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

exports.addCategoriasArt = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // Guardar el CLiente en la BD
  Laboratorio.addCategoriasArt(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la familia del artículo"
      })
    else res.send(data)

  })
};

exports.getCategoriasArtId = (req,res) => {
    Laboratorio.getCategoriasArtId(req.params.id,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

// Delete a users with the specified usersId in the request
exports.deleteCategoriasArt = (req, res) => {
  Laboratorio.deleteCategoriasArt(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};



exports.addFotoCategoria = async (req, res) => {
  try{

    if( !req.files ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
    }

    // desestrucutramos los arvhios cargados
    const { file } = req.files

    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    let extensiones = [ 'jpg', 'jpeg', 'png', 'webp', 'PNG', 'JPEG', 'gif', 'svg' ]

    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // Videos
    const  ruta        = './../../fotos-fetish/' + nombreUuid

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la extensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
    

    file.mv(`${ruta}`, async ( err ) => {
      if( err )
        return res.status( 400 ).send({ message: err, err })

      return res.send({ message: 'Imagen cargada correctamente', nombre: nombreUuid, extension: extensioArchivo })
    })
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al crear el video' } )
  }
};

/************************************************************************************************/

exports.getCatActivos = (req,res) => {
    Laboratorio.getCatActivos((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};



exports.getSubcategorias = (req,res) => {
    Laboratorio.getSubcategorias((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

exports.getSubcategoriasActivo = (req,res) => {
    Laboratorio.getSubcategoriasActivo((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

exports.addSubcategorias = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Laboratorio.addSubcategorias(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Laboratorio"
  		})
  	else res.send(data)

  })
};


exports.updateSubCatego = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Laboratorio.updateSubCatego(req.params.id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el laboratorio con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el laboratorio con el id" + req.params.id 
				});
			}
		} 
		else res.send(data);
	}
	);
}


exports.addsubCategoriasArt = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // Guardar el CLiente en la BD
  Laboratorio.addsubCategoriasArt(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la familia del artículo"
      })
    else res.send(data)

  })
};

exports.getsubCategoriasArtId = (req,res) => {
    Laboratorio.getsubCategoriasArtId(req.params.id,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

// Delete a users with the specified usersId in the request
exports.deletesubCategoriasArt = (req, res) => {
  Laboratorio.deletesubCategoriasArt(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};

/***********************************************************************************************/
// Traer laboratorios por id
exports.getSubcategoriasPorCatego = (req, res)=>{
    Laboratorio.getSubcategoriasPorCatego(req.params.idcatego,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

/***************************************************************************************************/
/*                                           FAMILIA                                               */
/***************************************************************************************************/

exports.addFamilias = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Laboratorio.addFamilias(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Laboratorio"
  		})
  	else res.send(data)

  })
};

exports.getFamilias = (req,res) => {
    Laboratorio.getFamilias((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

exports.updateFamilias = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Laboratorio.updateFamilias(req.params.id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el laboratorio con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el laboratorio con el id" + req.params.id 
				});
			}
		} 
		else res.send(data);
	}
	);
}


exports.getFamiliasActivo = (req,res) => {
    Laboratorio.getFamiliasActivo((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};


exports.addFamiliasArt = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // Guardar el CLiente en la BD
  Laboratorio.addFamiliasArt(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la familia del artículo"
      })
    else res.send(data)

  })
};

exports.getFamiliasArtId = (req,res) => {
    Laboratorio.getFamiliasArtId(req.params.id,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

// Delete a users with the specified usersId in the request
exports.deleteFamiliasArt = (req, res) => {
  Laboratorio.deleteFamiliasArt(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};



/***************************************************************************************************/
/*                                           TIENDA                                                */
/***************************************************************************************************/

exports.getMenuTienda = (req,res) => {
    Laboratorio.getMenuTienda((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};