
const Configuracion = require("../models/configuracion.model.js");

// Traer laboratorios 
exports.getConfiguracion = (req,res) => {
    Configuracion.getConfiguracion((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data[0]);
    });
};

// Actualizar un laboratorio
exports.updateConfig = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Configuracion.updateConfig(req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el ciaws con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el ciaws con el id" + req.params.id 
				});
			}
		} 
		else res.send(data);
	});
}







