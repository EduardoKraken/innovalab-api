
const Ciudades = require("../models/ciudades.model.js");

exports.obtener_ciudades_activas = (req, res)=>{
  Ciudades.obtener_ciudades_activas((err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún erro al recuperar las ciudades."
      });
      else res.status(200).send(data);
  });
};


