
const Clientes = require("../models/clientes.model.js");


exports.session_cliente = (req, res)=>{
  Clientes.login_cliente(req.body,(err, data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Este cliente no se encuentra registrado`
        });
      } else {
        res.status(500).send({
          message: "Error al buscar al cliente" 
        });
      }
    } else {
			console.log('data',data)
			if(data){
				res.status(200).send(data);
			}else{
				res.status(404).send({message:'ESTE USUARIO NO SE ENCUENTRA REGISTRADO'});
			}
		}
  });
};

// Crear un cliente
exports.addCliente = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Clientes.addCliente(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.status(200).send({ message:'El cliente se creo correctamente'})
  })
};

exports.getClienteId = (req, res)=>{
    Clientes.getClienteId(req.body,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

exports.getClientes = (req,res) => {
    Clientes.getClientes((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};

exports.updateCliente = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Clientes.updateCliente(req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el cliente con el id ${req.body.data.idcliente }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el cliente con el id" + req.body.data.idcliente 
				});
			}
		} 
		else res.status(200).send({ message: 'La información se actualizo correctamente.'});
	}
	);
}

exports.OlvideContraCliente = (req, res) => {
  Clientes.OlvideContraCliente(req.body,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(202).send({
          message: `No encontre el correo`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el correo" 
        });
      }
    } else res.send(data);
  });
};

exports.passwordExtraCliente = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Clientes.passwordExtraCliente(req.body,(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({
            message: `No encontre el cliente con el id ${req.body.id }.`
          });
        } else {
          res.status(500).send({
            message: "Error al actualizar el cliente con el id" + req.body.id 
          });
        }
      } 
      else res.send(data);
    }
  );
};







