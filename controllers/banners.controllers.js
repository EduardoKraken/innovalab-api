
const Banners = require("../models/banners.model.js");

// Agregar un laboratorio
exports.addBanner = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Banners.addBanner(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Laboratorio"
  		})
  	else res.send(data)

  })
};

// Traer laboratorios por id
exports.getBanners = (req, res)=>{
  Banners.getBanners((err,data)=>{
   if(err)
    res.status(500).send({
     message:
     err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
   });
  else res.send(data);
});
};

exports.getMarcas = (req, res)=>{
  Banners.getMarcas((err,data)=>{
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
      });
    else res.send(data);
  });
};

// Delete a users with the specified usersId in the request
exports.deleteBanner = (req, res) => {
  Banners.deleteBanner(req.params.idbanners, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.idbanners}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.idbanners
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};