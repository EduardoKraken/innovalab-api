const Mercadopago = require("../models/mercadopago.model.js");

// esta api, guardará todos los datos de las direcciones, y el pedido 
exports.mercadopagoAdd = async (req, res) => {
  try{

    // Primero guardaremos los datos de la direccion normal
    const pagoDirecciones    = req.body.direccion_envio
    const pagoDireccionesFac = req.body.direccion_factura
    const idcliente          = req.body.id_cliente

    // Paso #1, generar el pedido de mercado pago
    const mercadopago = req.body.mercadopago
    const idPago = await Mercadopago
      .mercadopagoAdd(mercadopago)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Paso #2 generar la direccion normal de la compra
    const idPagoDirecciones = await Mercadopago
      .agregarPagoDirecciones(pagoDirecciones,idcliente,idPago)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    let idPagoDireccionesFac = 0
    if(pagoDireccionesFac){
      // Paso #3, agregar la dirección de facturación
      idPagoDireccionesFac = await Mercadopago
        .agregarPagoDireccionesFac(pagoDireccionesFac,idcliente,idPago)
        .then((response) => response)
        .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));
    }else{
      idPagoDireccionesFac = 0
    }

    // Paso #4 generar el pedido, solo los datos generales del pedido
    const docum     = req.body.totales
    const idcupon   = req.body.idcupon.idcupon
    const idDocum   = await Mercadopago
      .agregarDocum(docum,idcliente,idPago,idPagoDirecciones,idPagoDireccionesFac,idcupon)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    // Paso #5 generar el movin, que es los artículos que se compraron
    const productos   = req.body.productos
    const idMovim     = await Mercadopago
      .agregarMovim(productos,idDocum)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    let respuestaFinal = {
      idPago:               idPago,
      idPagoDirecciones:    idPagoDirecciones,
      idPagoDireccionesFac: idPagoDireccionesFac,
      idDocum:              idDocum,
      idMovim:              idMovim
    }

    res.send(respuestaFinal);
  }catch(e){
    res.status(500).send({
      message:
      err.message || "Se produjo algún error al crea el pago"
    })
  }
};


exports.validarExistePago = (req, res) => {
  Mercadopago.validarExistePago(req.params.collection_id, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    else res.send(data);
  });
};

exports.addPagoMercadopago = (req, res) => {
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  Mercadopago.addPagoMercadopago(req.body, (err, data)=>{
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el cliente"
      })
    else res.send(data)
  })
};

exports.updateMercadoPago = async (req, res) => {
  try{
    const updateMovim = await Mercadopago
      .updateMercadoPago(req.params.idPago)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    const iddocum  = await Mercadopago
      .getIdMovim(req.params.idPago)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    const arts  = await Mercadopago
      .getArtsDocum(iddocum.iddocum)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    for(const i in arts){
      const updateArt  = await Mercadopago
        .updateArt(arts[i])
        .then((response) => response)
        .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));
      const addSalida  = await Mercadopago
        .addSalida(arts[i])
        .then((response) => response)
        .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));
    }

    res.send(arts);
  }catch(err){
    res.status(500).send({
      message:
      err.message || "Se produjo algún error al crea el pago"
    })
  }
};

exports.mercadopagoSuccess =  (req, res) => {
	console.log(req.params)
	res.send(req.params)
  // if(!req.body){
  // 	res.status(400).send({
  // 		message:"El Contenido no puede estar vacio"
  // 	});
  // }

  // Almacen.mercadopagoSuccess(req.body, (err, data)=>{
  // 	if(err)
  // 		res.status(500).send({
  // 			message:
  // 			err.message || "Se produjo algún error al crear el cliente"
  // 		})
  // 	else res.send(data)
  // })
};


exports.obtener_pedidos_cliente = (req, res) => {
  Mercadopago.obtener_pedidos_cliente(req.params.idcliente, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el pedido con id  ${req.params.idcliente}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el pedido con el id " + req.params.idcliente
        });
      }
    } else res.status(200).send(data);
  });
};

exports.obtener_productos_movim = (req, res) => {
  Mercadopago.obtener_productos_movim(req.params.iddocum, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el pedido con id  ${req.params.iddocum}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el pedido con el id " + req.params.iddocum
        });
      }
    } else res.status(200).send(data);
  });
};


// esta api, guardará todos los datos de las direcciones, y el pedido 
exports.deleteMercadoPedido = async (req, res) => {
  try{
    let idPagoDirecciones =    req.body.idPagoDirecciones
    let idPagoDireccionesFac = req.body.idPagoDireccionesFac
    let idDocum =              req.body.idDocum
    let idMovim =              req.body.idMovim

    // Paso #1, generar el pedido de mercado pago
    await Mercadopago
      .deleteDirecciones(idPagoDirecciones)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    await Mercadopago
      .deleteDireccionesFac(idPagoDireccionesFac)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    await Mercadopago
      .deleteDocum(idDocum)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    await Mercadopago
      .deleteMovim(idDocum)
      .then((response) => response)
      .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

    res.send({message: 'Listo'});
  }catch(e){
    res.status(500).send({
      message:
      err.message || "Se produjo algún error al crea el pago"
    })
  }
};


exports.getRastreo = (req, res) => {
  Mercadopago.getRastreo(req.params.idpago, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    else res.send(data);
  });
};

exports.addRastreo = (req, res) => {
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }
  Mercadopago.addRastreo(req.body, (err, data)=>{
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el cliente"
      })
    else res.send(data)
  })
};