const Articulos = require("../models/articulos.model.js");
// const Fotos = require("../models/articulos.model.js");
var articulos = []

exports.obtener_articulos_random = (req, res)=>{
  Articulos.obtener_articulos_random((err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else {
      res.status(200).send(data);
    }
      
  });
};

exports.obtener_articulos = (req, res)=>{
  Articulos.obtener_articulos((err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else {
      res.status(200).send(data);
    }
      
  });
};

// Crear un cliente
exports.addArticulos = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Articulos.addArticulos(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};


exports.findAll =  (req,res)=>{
  Articulos.getAll((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
      // else res.send(data);
    }else res.send(data);
  });
};


exports.getArticuloId =  (req,res)=>{
  Articulos.getArticuloId(req.params.id,(err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
      // else res.send(data);
    }else res.send(data);
  });
};


exports.getArticulosMovil =  (req,res)=>{
  Articulos.getAll((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
      // else res.send(data);
    }else{
      res.send(data)
    } 
  });
};

fotos = (callback) =>{
	Articulos.fotosArt((err, data) => {
    if (err){
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    }else{
    	callback(data);
    }
  });
};

exports.getArticuloCodigo = (req, res)=>{
    Articulos.getArticuloCodigo(req.params.codigo,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo un error."
				});
      else res.send(data);
				
    });
};

exports.getArticuloTienda = (req, res)=>{
    Articulos.getArticuloTienda(req.params.codigo,(err,data)=>{
      if(err)
        res.status(500).send({
          message:
            err.message || "Se produjo un error."
        });
        else res.send(data);
    });
};


exports.updateArticulos = (req, res) =>{
 if (!req.body) {
   res.status(400).send({
     message: "El Contenido no puede estar vacio!"
   });
 }
  
 Articulos.updateArticulos(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el articulo con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el articulo con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};

// Traer fotos por articulo
exports.fotosxArt = (req, res)=>{
    Articulos.fotosxArt(req.params.codigo,(err,data)=>{
      if(err)
        res.status(500).send({
          message:
            err.message || "Se produjo un error."
        });
        else res.send(data);
    });
};

// Agregar fotos por codigo
exports.addFoto = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // console.log(req.body)
  // req.body.forEach(element=>{
    // Guardar el CLiente en la BD
    Articulos.addFoto(req.body, (err, data)=>{
      // EVALUO QUE NO EXISTA UN ERROR
      if(err)
        res.status(500).send({
          message:
          err.message || "Se produjo algún error al crear el cliente"
        })
      else res.send(data)
    // })
  })
};

exports.updateNovedades = (req, res) =>{
 if (!req.body) {
   res.status(400).send({
     message: "El Contenido no puede estar vacio!"
   });
 }
  
 Articulos.updateNovedades(req.params.id, req.body ,(err, data) => {
   if (err) {
     if (err.kind === "not_found") {
       res.status(404).send({
       message: `No encontre el articulo con el id ${req.params.id }.`
       });
     } else {
       res.status(500).send({
       message: "Error al actualizar el articulo con el id" + req.params.id 
       });
     }
   } 
   else res.send(data);
 });
};


exports.getArticulosActivo = (req, res)=>{
  Articulos.getArticulosActivo((err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      else res.send(data);
  });
};

exports.getNovedades = (req, res)=>{
    Articulos.getNovedades((err,data)=>{
      if(err)
        res.status(500).send({
          message:
            err.message || "Se produjo un error."
        });
        fotos((result)=> {
          articulos = []
          data.forEach((element, index) =>{
            articulos.push({             
              id            : element.id,
              nomart        : element.nomart,
              codigo        : element.codigo,
              idlaboratorio : element.idlaboratorio,
              nomlab        : element.nomlab,
              descrip       : element.descrip,
              statusweb     : element.statusweb,
              precio1       : element.precio1,
              sal           : element.sal,
              destacados    : element.destacados,
              novedades     : element.novedades,
              fotos         : [],
              pjedesc       : element.pjedesc,
            });

            result.forEach(element2 =>{
              if(element.codigo == element2.codigo){
                console.log('entre')
                articulos[index].fotos.push(element2)
                console.log('articulo con foto: ',articulos[index])
              }             
            })

            if(index == data.length -1){
              res.send(articulos)
            }
          })
        });
    });
};

exports.getDestacados = (req, res)=>{
  Articulos.getDestacados((err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      fotos((result)=> {
        articulos = []
        data.forEach((element, index) =>{
          articulos.push({             
            id            : element.id,
            nomart        : element.nomart,
            codigo        : element.codigo,
            idlaboratorio : element.idlaboratorio,
            nomlab        : element.nomlab,
            descrip       : element.descrip,
            statusweb     : element.statusweb,
            precio1       : element.precio1,
            sal           : element.sal,
            destacados    : element.destacados,
            novedades     : element.novedades,
            fotos         : [],
            pjedesc       : element.pjedesc,
          });

          result.forEach(element2 =>{
            if(element.codigo == element2.codigo){
              console.log('entre')
              articulos[index].fotos.push(element2)
              console.log('articulo con foto: ',articulos[index])
            }             
          })

          if(index == data.length -1){
            res.send(articulos)
          }
        })
      });
  });
};

exports.getPromociones = (req, res)=>{
  Articulos.getPromociones((err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      fotos((result)=> {
        articulos = []
        data.forEach((element, index) =>{
          articulos.push({             
           id            : element.id,
           nomart        : element.nomart,
           codigo        : element.codigo,
           idlaboratorio : element.idlaboratorio,
           nomlab        : element.nomlab,
           descrip       : element.descrip,
           statusweb     : element.statusweb,
           precio1       : element.precio1,
           sal           : element.sal,
           fotos         : [],
           pjedesc       : element.pjedesc,
          });

          result.forEach(element2 =>{
            if(element.codigo == element2.codigo){
              console.log('entre')
              articulos[index].fotos.push(element2)
              console.log('articulo con foto: ',articulos[index])
            }             
          })

          if(index == data.length -1){
            res.send(articulos)
          }
        })
      });
  });
};

exports.getArtxLab = (req, res)=>{
  Articulos.getArtxLab(req.params.id,(err,data)=>{
    if(err)
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
      fotos((result)=> {
        articulos = []
        data.forEach((element, index) =>{
          articulos.push({             
           id            : element.id,
           nomart        : element.nomart,
           codigo        : element.codigo,
           idlaboratorio : element.idlaboratorio,
           nomlab        : element.nomlab,
           descrip       : element.descrip,
           statusweb     : element.statusweb,
           precio1       : element.precio1,
           sal           : element.sal,
           fotos         : [],
           pjedesc       : element.pjedesc,
          });

          result.forEach(element2 =>{
            if(element.codigo == element2.codigo){
              console.log('entre')
              articulos[index].fotos.push(element2)
              console.log('articulo con foto: ',articulos[index])
            }             
          })

          if(index == data.length -1){
            res.send(articulos)
          }
        })
      });
  });
};


exports.obtener_novedades = (req, res)=>{
  Articulos.obtener_novedades((err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.status(200).send(data)
    }
  });
};

exports.obtener_destacados = (req, res)=>{
Articulos.obtener_destacados((err,data)=>{
  if(err){
    res.status(500).send({
      message:
        err.message || "Se produjo un error."
    });
  }else{
    res.status(200).send(data)
  }
    
});
};

exports.obtener_juguetes = (req, res)=>{
Articulos.obtener_juguetes((err,data)=>{
  if(err){
    res.status(500).send({
      message:
        err.message || "Se produjo un error."
    });
  }else{
    res.status(200).send(data)
  }
    
});
};



// Delete a users with the specified usersId in the request
exports.deleteFoto = (req, res) => {
  Articulos.deleteFoto(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};


/*******************************************************************************/
exports.updateFotoPrincipal = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
 Articulos.updateFotoPrincipal(req.params.id, req.body ,(err, data) => {
  if (err) {
    if (err.kind === "not_found") {
      res.status(404).send({
        message: `No encontre el articulo con el id ${req.params.id }.`
      });
    } else {
      res.status(500).send({
        message: "Error al actualizar el articulo con el id" + req.params.id 
      });
    }
  } 
  else res.send(data);
 });
};


exports.getArticuloFotos =  (req, res)=>{
  Articulos.getArticuloFotos(req.params.codigo,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};


exports.getArticuloFam =  async (req, res)=>{
  const id = req.params.id

  const fam = await Articulos
    .getArticuloFam(id)
    .then((response) => response)
    .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

  const cat = await Articulos
    .getArticuloCat(id)
    .then((response) => response)
    .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

  const sub = await Articulos
    .getArticuloSub(id)
    .then((response) => response)
    .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

  var data = fam.concat(cat)
  data = data.concat(sub)
  res.send(data);
};

exports.getArticuloCat =  (req, res)=>{
  Articulos.getArticuloCat(req.params.id,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};

exports.getArticuloSub =  (req, res)=>{
  Articulos.getArticuloSub(req.params.id,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};


exports.getArticuloxFam =  (req, res)=>{
  Articulos.getArticuloxFam(req.params.id,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};

exports.getArticuloxCat =  (req, res)=>{
  Articulos.getArticuloxCat(req.params.id,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};

exports.getArticuloxSub =  (req, res)=>{
  Articulos.getArticuloxSub(req.params.id,(err,data)=>{
    if(err){
      res.status(500).send({
        message:
          err.message || "Se produjo un error."
      });
    }else{
      res.send(data);
    }
  });
};


exports.getRelacionados =  async (req, res)=>{
  const id = req.params.id

  const cat = await Articulos
    .getRelacionadosCat(id)
    .then((response) => response)
    .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

  const sub = await Articulos
    .getRelacionadosSub(id)
    .then((response) => response)
    .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

  var data = cat.concat(sub)
  res.send(data);
};