module.exports = app => {
  const banners = require('../controllers/banners.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/banner.add",     							banners.addBanner);
  app.get("/banner.list",     							banners.getBanners);
  app.delete("/banner.delete/:idbanners", 	banners.deleteBanner);


  app.get("/marcas.list",                   banners.getMarcas);

};