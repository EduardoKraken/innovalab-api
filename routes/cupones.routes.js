module.exports = app => {
  const Cupones = require('../controllers/cupones.controllers') // --> ADDED THIS

  app.post("/validar.cupon.existente",   Cupones.validar_cupon_existente); 
  app.post("/cupones.add",               Cupones.addCupon); 
  app.get("/cupones.all",                Cupones.getCupones); 
  app.put("/cupones.update/:id",         Cupones.updateCupon); 


};