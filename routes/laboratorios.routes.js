module.exports = app => {
  const laboratorios = require('../controllers/laboratorios.controllers') // --> ADDED THIS

  // Rutas para el administrador
  app.get("/categorias.all"            			  , laboratorios.getCategorias);   // BUSCAR 
  app.post("/categorias.add"		              , laboratorios.addCategorias);   // BUSCAR 
  app.put("/categorias.update/:id"      	    , laboratorios.updateCategorias);
  app.get("/categorias.activo"                , laboratorios.getCategoriasActivo);   // BUSCAR 
  app.get("/categorias.familia/:id"           , laboratorios.getCategoriaPorFamilia);   // BUSCAR
  app.get("/categorias.art.id/:id"            , laboratorios.getCategoriasArtId);
  app.post("/categorias.art.add"              , laboratorios.addCategoriasArt);
  app.delete("/categorias.art.delete/:id"     , laboratorios.deleteCategoriasArt); 
  app.post("/categorias.foto"                 , laboratorios.addFotoCategoria); 

  // Rutas para las subcategorias
  app.get("/subcategorias.all"                    , laboratorios.getSubcategorias);   // BUSCAR 
  app.post("/subcategorias.add"                   , laboratorios.addSubcategorias);   // BUSCAR 
  app.put("/subcategorias.update/:id"             , laboratorios.updateSubCatego);
  app.get("/subcategorias.categoria/:idcatego"    , laboratorios.getSubcategoriasPorCatego);   // BUSCAR 
  app.get("/subcategorias.activo"                 , laboratorios.getSubcategoriasActivo);   // BUSCAR 
  app.get("/subcategorias.art.id/:id"             , laboratorios.getsubCategoriasArtId);
  app.post("/subcategorias.art.add"               , laboratorios.addsubCategoriasArt);
  app.delete("/subcategorias.art.delete/:id"      , laboratorios.deletesubCategoriasArt);

  /***************************************************************************************************/
  /*                                           FAMILIA                                               */
  /***************************************************************************************************/

  app.get("/familias.all"                   , laboratorios.getFamilias); 
  app.post("/familias.add"                  , laboratorios.addFamilias); 
  app.put("/familias.update/:id"            , laboratorios.updateFamilias); 
  app.get("/familias.activo"                , laboratorios.getFamiliasActivo); 
  app.get("/familias.art.id/:id"            , laboratorios.getFamiliasArtId);
  app.post("/familias.art.add"              , laboratorios.addFamiliasArt);
  app.delete("/familias.art.delete/:id"     , laboratorios.deleteFamiliasArt);


  /***************************************************************************************************/
  /*                                           TIENDA                                                */
  /***************************************************************************************************/

  app.get("/menu.tienda"            , laboratorios.getMenuTienda); 


};