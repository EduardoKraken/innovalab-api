module.exports = app => {
  const depositos = require('../controllers/depositos.controllers') // --> ADDED THIS

  app.post("/pagoxfac.add",                depositos.addDepositos);   // CREAR UN NUEVO GRUPO
  app.get("/pagoxfac/:idpagoxfac",         depositos.getDepositosId);   
  app.get("/pagoxfac.list/:idfacturas",   depositos.getDepositos);   
  app.put("/pagoxfac.update",              depositos.updateDepositos);

};